## Solar panel dashboard

A dashboard to monitor solar farm energy production.
Solar farms consist of 30 solar panels, has a server that is able to provide voltage and wattage per solar panel.
REST forecast data service is provided separately.

Before starting a project:
- set up PlanetOS account and go to [Account Info -> API info](https://data.planetos.com/account/settings/). Copy API Key
- create file with a name `.env.local` in a root directory
- set `REACT_APP_PLANETOS_API_KEY` to the API key you just copied


Start a project  
`yarn start`

Run tests  
`yarn test`

Build a project  
`yarn build`