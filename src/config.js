export const planetOsApi = {
  url: 'http://api.planetos.com/v1/datasets/',
  query: 'bom_access-g_global_40km/point',
  pollInterval: 5 * 60 * 1000
}

export const solarPanelsConfig = {
  pollInterval: 10 * 1000
}

const commonParams = {
  apikey: `${process.env.REACT_APP_PLANETOS_API_KEY}`,
  lon: -50.5,
  lat: 49.5,
  count: 50
}

export const solarActivityParams = {
  ...commonParams,
  var: 'av_swsfcdown',
}

export const cloudCoverageParams = {
  ...commonParams,
  var: 'av_ttl_cld'
}