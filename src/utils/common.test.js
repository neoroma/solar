import * as Utils from './common'
import { 
  solarPanelsMock, 
  solarActivityMock, 
  cloudCoverageMock 
} from '../__mocks__'
import * as R from 'ramda'
import { toDate, format } from 'date-fns'


describe('Utils', () => {
  describe('constrain', () => {
    beforeEach(() => {
      global.Math.random = jest.fn().mockReturnValue(0.5)
    })
  
    afterEach(() => jest.resetAllMocks())
  
    it('generates random', () => {
      expect(Utils.constrain(80, 200)).toBe(140)
    })
  })
  
  describe('getSolarPanelsTotalOutput', () => {
    it('calculates total solar panels output', () => {
      expect(Utils.getSolarPanelsTotalOutput(solarPanelsMock)).toBe(4.403)
    })
  })
  
  describe('generateSolarPanelsData', () => {
    beforeEach(() => {
      global.Math.random = jest.fn().mockReturnValue(0.5)
    })
  
    afterEach(() => jest.resetAllMocks())
  
    it('generates array of solar panels with correct length', () => {
      expect(Utils.generateSolarPanelsData(30)).toHaveLength(30)
    })
  
    it('generates data of correct format and type', () => {
      const expected = {
        id: expect.stringMatching(/^sp-\d+$/),
        wattage: expect.any(Number),
        voltage: expect.any(Number)
      }
      
      const result = Utils.generateSolarPanelsData(30)
  
      expect(result).toEqual(expect.arrayContaining([
        expect.objectContaining(expected),
        expect.objectContaining({ 
          id: 'sp-1', 
          voltage: 225, 
          wattage: 126 
        })
      ]))
    })
  })
  
  describe('isWithinNext24Hours', () => {
    let dateNowSpy;
  
    beforeAll(() => {
      dateNowSpy = jest.spyOn(Date, 'now').mockImplementation(() => 1568469142730);
    });
  
    afterAll(() => {
      dateNowSpy.mockRestore();
    });
  
    it('returns true if within next 24 hours', () => {
      expect(Utils.isWithinNext24Hours('2019-09-14T16:52:23')).toBeTruthy()
      expect(Utils.isWithinNext24Hours('2019-09-15T16:52:21')).toBeTruthy()
    })
  
    it('returns false if outside of next 24 hours period', () => {
      expect(Utils.isWithinNext24Hours('2019-09-14T16:52:21')).toBeFalsy()
      expect(Utils.isWithinNext24Hours('2019-09-15T16:52:23')).toBeFalsy()
    })
  })
  
  describe('getXAxisTicks', () => {
    let dateNowSpy;
  
    beforeAll(() => {
      dateNowSpy = jest.spyOn(Date, 'now').mockImplementation(() => 1568469142730);
    });
  
    afterAll(() => {
      dateNowSpy.mockRestore();
    });
  
    it('returns x axis ticks array of correct length', () => {
      expect(Utils.getXAxisTicks(25)).toHaveLength(25)
    })
  
    it('returns correct first tick time', () => {
      const [firstTick] = Utils.getXAxisTicks(25)
      const tickString = format(toDate(firstTick), 'HH:mm')
      expect(tickString).toEqual('16:00')
    })
  
    it('returns correct last tick time', () => {
      const ticks = Utils.getXAxisTicks(25)
      const lastTick = R.last(ticks)
      const lastTickTimeString = format(toDate(lastTick), 'HH:mm')
      expect(lastTickTimeString).toEqual('16:00')
    })
  })
  
  describe('tickFormatter', () => {
    it('outputs time in HH:mm format', () => {
      expect(Utils.tickFormatter(1568469142730)).toEqual('16:52')
    })
  })
  
  describe('getXAxisDomain', () => {
    let dateNowSpy;
    let solarActivityData;
    let domain;
  
    beforeAll(() => {
      dateNowSpy = jest.spyOn(Date, 'now').mockImplementation(() => 1568278800000);
      solarActivityData = Utils.getSolarActivity(solarActivityMock.entries)
      domain = Utils.getXAxisDomain(solarActivityData)
    });
  
    afterAll(() => {
      dateNowSpy.mockRestore();
    });
    
    it('returst start and end domain with correct types', () => {
      expect(domain).toEqual(
        expect.objectContaining({
          domainStart: expect.any(Number),
          domainEnd: expect.any(Number)
        })
      )
    })
  
    it('returst correct start and end domain', () => {
      expect(domain).toEqual({
        domainEnd: 1568365200000, 
        domainStart: 1568278800000
      })
    })
  })
  
  
  describe('getSolarActivity', () => {
    let dateNowSpy;
    let solarActivity
  
    beforeAll(() => {
      dateNowSpy = jest.spyOn(Date, 'now').mockImplementation(() => 1568278800000);
      solarActivity = Utils.getSolarActivity(solarActivityMock.entries)
    });
  
    afterAll(() => {
      dateNowSpy.mockRestore();
    });
  
    it('outputs array of objects with data of correct type', () => {
      expect(solarActivity).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            time: expect.any(Number),
            watts: expect.any(Number)
          })
        ])
      )
    })
  
    it('outputs correct data for one entry', () => {
      expect(R.head(solarActivity)).toEqual({ 
        time: 1568278800000, 
        watts: 8 
      })
    })
  })
  
  describe('getCloudCoverage', () => {
    let dateNowSpy;
    let cloudCoverage
  
    beforeAll(() => {
      dateNowSpy = jest.spyOn(Date, 'now').mockImplementation(() => 1568278800000);
      cloudCoverage = Utils.getCloudCoverage(cloudCoverageMock.entries)
    });
  
    afterAll(() => {
      dateNowSpy.mockRestore();
    });
  
    it('outputs array of objects with data of correct type', () => {
      expect(cloudCoverage).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            time: expect.any(Number),
            percentage: expect.any(Number)
          })
        ])
      )
    })
  
    it('outputs correct data for one entry', () => {
      expect(R.head(cloudCoverage)).toEqual({ 
        time: 1568332800000, 
        percentage: 100
      })
    })
  })  
})
