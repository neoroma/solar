import * as R from 'ramda'
import { addHours, isWithinInterval, parseISO, format, set } from 'date-fns'

export const constrain = (min, max) => Math.floor(Math.random() * (max - min + 1) + min)
const toKilowatts = watts => watts / 1000

const mapPanelToObject = (panelId) => ({
  id: `sp-${panelId}`,
  voltage: constrain(200, 250),
  wattage: constrain(1, 250)
})

export const generateSolarPanelsData = R.compose(
  R.map(mapPanelToObject), 
  R.range(0)
)

export const getSolarPanelsTotalOutput = R.compose(
  toKilowatts, 
  R.sum, 
  R.pluck('wattage')
)

export const isWithinNext24Hours = (timeString) => {
  const now = Date.now()
  return isWithinInterval(parseISO(timeString), { 
    start: now, 
    end: addHours(now, 24)
  })
}

const toTimestamp = date => Number(format(date, "T"));
const addNow = hour => addHours(Date.now(), hour);
const resetMinutesAndSeconds = date => set(date, { minutes: 0, seconds: 0})
const timePath = R.path(['axes', 'time'])
export const tickFormatter = tick => format(tick, "HH:mm")
export const getXAxisTicks = R.compose(
  R.map(
    R.compose(toTimestamp, resetMinutesAndSeconds, addNow)
  ),
  R.range(0)
);

export const getXAxisDomain = (data) => {
  const timeProp = R.prop('time')
  return {
    domainStart: R.compose(timeProp, R.head)(data),
    domainEnd: R.compose(timeProp, R.last)(data)
  }
}

const mapSolarActivityEntryToObject = entry => ({
  time: toTimestamp(parseISO(entry.axes.time)),
  watts: Math.floor(entry.data.av_swsfcdown)
});

const mapCloudCoverageEntryToObject = entry => ({
  time: toTimestamp(parseISO(entry.axes.time)),
  percentage: entry.data.av_ttl_cld * 100,
})

export const selectData =  R.compose(
  R.filter(
    R.compose(isWithinNext24Hours, timePath)
  ), 
  R.take(24)
)
export const getSolarActivity = R.compose(R.map(mapSolarActivityEntryToObject), selectData)
export const getCloudCoverage = R.compose(R.map(mapCloudCoverageEntryToObject), selectData)