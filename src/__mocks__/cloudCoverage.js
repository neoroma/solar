export const cloudCoverageMock = {
    "stats": {
        "timeMin": "2019-09-13T03:00:00",
        "count": 50,
        "offset": 0,
        "nextOffset": 50,
        "timeMax": "2019-09-23T00:00:00"
    },
    "entries": [
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-13T03:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 1.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-13T06:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.71875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-13T09:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.078125
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-13T12:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.40625
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-13T15:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.609375
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-13T18:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.265625
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-13T21:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.84375
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-14T00:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.6875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-14T03:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.8125
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-14T06:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.921875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-14T09:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.078125
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-14T12:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.109375
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-14T15:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-14T18:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-14T21:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-15T00:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-15T03:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.203125
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-15T06:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.765625
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-15T09:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.6875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-15T12:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.828125
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-15T15:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.4375
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-15T18:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.09375
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-15T21:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.140625
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-16T00:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.40625
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-16T03:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.609375
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-16T06:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 1.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-16T09:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 1.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-16T12:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 1.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-16T15:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 1.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-16T18:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 1.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-16T21:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 1.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-17T00:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 1.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-17T03:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 1.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-17T06:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 1.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-17T09:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 1.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-17T12:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 1.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-17T15:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 1.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-17T18:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 1.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-17T21:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 1.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-18T00:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 1.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-18T03:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.984375
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-18T06:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-18T09:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.828125
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-18T12:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.9375
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-18T15:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.96875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-18T18:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 1.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-18T21:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 1.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-19T00:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 1.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-19T03:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.921875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-19T06:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_ttl_cld": 0.96875
            }
        }
    ]
}