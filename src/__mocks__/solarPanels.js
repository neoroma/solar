export const solarPanelsMock = [
  {
    "id": "sp-0",
    "voltage": 249,
    "wattage": 246
  },
  {
    "id": "sp-1",
    "voltage": 220,
    "wattage": 37
  },
  {
    "id": "sp-2",
    "voltage": 211,
    "wattage": 224
  },
  {
    "id": "sp-3",
    "voltage": 244,
    "wattage": 174
  },
  {
    "id": "sp-4",
    "voltage": 211,
    "wattage": 68
  },
  {
    "id": "sp-5",
    "voltage": 227,
    "wattage": 90
  },
  {
    "id": "sp-6",
    "voltage": 241,
    "wattage": 155
  },
  {
    "id": "sp-7",
    "voltage": 239,
    "wattage": 249
  },
  {
    "id": "sp-8",
    "voltage": 243,
    "wattage": 170
  },
  {
    "id": "sp-9",
    "voltage": 230,
    "wattage": 42
  },
  {
    "id": "sp-10",
    "voltage": 230,
    "wattage": 219
  },
  {
    "id": "sp-11",
    "voltage": 213,
    "wattage": 104
  },
  {
    "id": "sp-12",
    "voltage": 204,
    "wattage": 128
  },
  {
    "id": "sp-13",
    "voltage": 228,
    "wattage": 236
  },
  {
    "id": "sp-14",
    "voltage": 249,
    "wattage": 48
  },
  {
    "id": "sp-15",
    "voltage": 214,
    "wattage": 224
  },
  {
    "id": "sp-16",
    "voltage": 219,
    "wattage": 221
  },
  {
    "id": "sp-17",
    "voltage": 230,
    "wattage": 108
  },
  {
    "id": "sp-18",
    "voltage": 216,
    "wattage": 111
  },
  {
    "id": "sp-19",
    "voltage": 226,
    "wattage": 223
  },
  {
    "id": "sp-20",
    "voltage": 235,
    "wattage": 74
  },
  {
    "id": "sp-21",
    "voltage": 210,
    "wattage": 181
  },
  {
    "id": "sp-22",
    "voltage": 215,
    "wattage": 3
  },
  {
    "id": "sp-23",
    "voltage": 229,
    "wattage": 229
  },
  {
    "id": "sp-24",
    "voltage": 234,
    "wattage": 89
  },
  {
    "id": "sp-25",
    "voltage": 229,
    "wattage": 242
  },
  {
    "id": "sp-26",
    "voltage": 220,
    "wattage": 149
  },
  {
    "id": "sp-27",
    "voltage": 206,
    "wattage": 209
  },
  {
    "id": "sp-28",
    "voltage": 217,
    "wattage": 105
  },
  {
    "id": "sp-29",
    "voltage": 239,
    "wattage": 45
  }
]