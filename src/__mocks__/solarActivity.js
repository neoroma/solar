export const solarActivityMock = {
    "stats": {
        "timeMin": "2019-09-12T12:00:00",
        "count": 100,
        "offset": 0,
        "nextOffset": 50,
        "timeMax": "2019-09-23T00:00:00"
    },
    "entries": [
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-12T12:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 8.773981094360352
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-12T15:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 47.207763671875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-12T18:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 77.93505859375
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-12T21:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 103.81927490234375
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-13T00:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 1.67041015625
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-13T03:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-13T06:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-13T09:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-13T12:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 179.0462646484375
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-13T15:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 609.0442504882812
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-13T18:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 505.65966796875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-13T21:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 149.8656005859375
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-14T00:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 3.6097412109375
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-14T03:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-14T06:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-14T09:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-14T12:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 176.3350830078125
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-14T15:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 439.0057373046875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-14T18:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 647.1229248046875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-14T21:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 302.1607666015625
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-15T00:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 4.4959716796875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-15T03:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-15T06:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-15T09:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-15T12:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 109.39794921875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-15T15:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 376.07476806640625
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-15T18:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 538.0916748046875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-15T21:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 242.71875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-16T00:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 3.0927734375
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-16T03:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-16T06:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-16T09:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-16T12:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 26.562744140625
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-16T15:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 176.9874267578125
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-16T18:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 247.54833984375
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-16T21:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 87.28070068359375
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-17T00:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 1.02587890625
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-17T03:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-17T06:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-17T09:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-17T12:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 65.205810546875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-17T15:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 282.25006103515625
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-17T18:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 414.215576171875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-17T21:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 169.814697265625
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-18T00:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 1.1077880859375
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-18T03:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-18T06:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-18T09:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-18T12:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 139.21923828125
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-12T12:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-12T12:00:00",
                "time": "2019-09-18T15:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 487.84381103515625
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-13T06:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-13T09:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-13T12:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 176.884033203125
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-13T15:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 507.16278076171875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-13T18:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 646.8092041015625
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-13T21:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 191.72747802734375
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-14T00:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 3.5020751953125
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-14T03:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-14T09:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-14T12:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 203.0921630859375
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-14T18:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 669.8984375
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-14T21:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 302.12078857421875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-15T03:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-15T09:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-15T12:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 151.310546875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-15T15:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 583.5625
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-15T18:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 636.435302734375
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-16T00:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 3.1573486328125
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-16T06:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-16T09:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-16T12:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 34.7420654296875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-16T15:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 170.0543212890625
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-16T18:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 188.3133544921875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-16T21:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 69.0850830078125
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-17T00:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.6505126953125
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-17T03:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-17T06:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-17T12:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 53.498046875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-17T15:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 180.7342529296875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-17T18:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 221.9578857421875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-17T21:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 77.30474853515625
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-18T00:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.772216796875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-18T03:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-18T06:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-18T15:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 146.3876953125
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-18T18:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 81.89306640625
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-18T21:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 56.9189453125
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-19T00:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.6099853515625
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-19T03:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-19T06:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-19T09:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-19T15:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 324.9488525390625
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-19T18:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 188.9072265625
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-19T21:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 228.7117919921875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-20T03:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-20T06:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-20T09:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 0.0
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-20T12:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 59.423828125
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-20T15:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 119.768310546875
            }
        },
        {
            "context": "reference_time_time_lat_lon",
            "classifiers": {
                "reference_time": "2019-09-13T00:00:00.000Z"
            },
            "axes": {
                "reftime": "2019-09-13T00:00:00",
                "time": "2019-09-20T18:00:00",
                "latitude": 49.453125,
                "longitude": -50.625
            },
            "data": {
                "av_swsfcdown": 122.4725341796875
            }
        }
    ]
}