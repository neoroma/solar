import { solarPanelsMock } from './solarPanels'
import { solarActivityMock } from './solarActivity'
import { cloudCoverageMock } from './cloudCoverage'

export {
  cloudCoverageMock,
  solarActivityMock,
  solarPanelsMock
}