import React from 'react'
import axios from 'axios'

function reducer(state, action) {
  switch(action.type) {
    case 'poll': 
      return {
        ...state,
        isFetching: true,
        requestId: state.requestId + 1
      }
    case 'fetched':
      return {
        ...state,
        data: action.payload,
        isFetching: false
      }
    case 'error': 
      return {
        ...state,
        isFetching: false,
        error: action.payload
      }
    default:
      return state
  }
}

export function useRequest(url, params, pollInterval) {
  const [state, dispatch] = React.useReducer(reducer, {
    url,
    data: null,
    isFetching: true,
    error: null,
    requestId: 1
  })

  const dispatchPoll = React.useCallback(() => {
    dispatch({ type: 'poll' });
  }, [dispatch]);

  const dispatchFetched = React.useCallback((data) => {
    dispatch({ type: 'fetched', payload: data });
  }, [dispatch]);

  const dispatchError = React.useCallback((error) => {
    dispatch({ type: 'error', payload: error });
  }, [dispatch]);

  React.useEffect(() => {
    axios.get(state.url, { params })
      .then(({ data }) => {
        dispatchFetched(data)
      }).catch(error => {
        dispatchError(error)
      })
  }, [params, dispatchError, dispatchFetched, state.url, state.requestId])

  React.useEffect(() => {
    if (!pollInterval || state.isFetching) return;
    const timeoutId = setTimeout(dispatchPoll, pollInterval);

    return () => {
      clearTimeout(timeoutId);
    };
  }, [pollInterval, state.isFetching, dispatchPoll]);

  return {
    isFetching: state.isFetching,
    requestId: state.requestId,
    data: state.data,
    error: state.error
  };
}