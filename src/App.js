import React from 'react';
import { SolarPanels } from './containers/SolarPanels';
import { WeatherForecast } from './containers/WeatherForecast';
import { ThemeProvider, Box } from "@chakra-ui/core";
import { customTheme } from './theme'
import { CSSReset } from "@chakra-ui/core";

export function App() {
  return (
    <ThemeProvider theme={customTheme}>
      <CSSReset />
      <Box 
        maxWidth="1200px" 
        margin="0 auto">
        <WeatherForecast />
        <SolarPanels />
      </Box>
    </ThemeProvider>
  );
}
