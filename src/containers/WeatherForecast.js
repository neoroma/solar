import React from 'react'
import { SolarActivityChart } from '../components/SolarActivityChart/SolarActivityChart';
import { CloudCoverageChart } from '../components/CloudCoverageChart/CloudCoverageChart';
import { useWindowSize } from '../hooks/useWindowSize';
import { Box } from '@chakra-ui/core'

export function WeatherForecast() {
  const size = useWindowSize();
  const tickInterval = size.innerWidth < 768 ? 5 : 3 

  return (
    <Box
      display="flex"
      flexWrap="nowrap"
      flexDirection={['column', null, 'row']}>
      <Box 
        width={['100%', null, '50%']}
        height="250px">
        <SolarActivityChart tickInterval={tickInterval}/>
      </Box>
      <Box 
        width={['100%', null, '50%']}
        height="250px">
        <CloudCoverageChart tickInterval={tickInterval}/>  
      </Box>
    </Box>
  )
}