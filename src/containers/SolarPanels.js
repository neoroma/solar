import React from 'react'
import { SolarPanelList } from '../components/SolarPanelList/SolarPanelList'
import { SolarPanelListSummary } from '../components/SolarPanelListSummary/SolarPanelListSummary'
import { getSolarPanelsTotalOutput, generateSolarPanelsData } from '../utils/common'
import { solarPanelsConfig } from '../config'

export function SolarPanels() {
  const [state, setState] = React.useState({    
    totalOutput: 0,
    solarPanels: []
  })

  React.useEffect(()=> {
    const data = generateSolarPanelsData(30)
    setState({ 
      solarPanels: generateSolarPanelsData(30), 
      totalOutput: getSolarPanelsTotalOutput(data) 
    })

    const timerId = setInterval(() => {
      const data = generateSolarPanelsData(30)
      setState({ 
        solarPanels: data, 
        totalOutput: getSolarPanelsTotalOutput(data) 
      })
    }, solarPanelsConfig.pollInterval)

    return () => {
      clearInterval(timerId)
    }
  }, [])

  return (
    <>
      <SolarPanelListSummary totalOutput={state.totalOutput} />
      <SolarPanelList solarPanels={state.solarPanels} />
    </>
  )
}