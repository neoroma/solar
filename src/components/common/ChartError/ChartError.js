import React from 'react'
import PropTypes from 'prop-types'
import { Icon, Flex, Text } from '@chakra-ui/core'

export function ChartError({ errorMessage }) {
  return (
    <Flex alignItems="center" justifyContent="center" height="100%">
      <Icon name="warning" size="32px" color="red.500" />
      <Text ml="1rem" fontSize={['sm', 'xl', 'lg']} color="tomato">
        {errorMessage}
      </Text>
    </Flex>
  )
}

ChartError.propTypes = {
  errorMessage: PropTypes.string.isRequired
}