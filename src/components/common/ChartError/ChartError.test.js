import React from 'react'
import { render } from '@testing-library/react'
import { ChartError } from './ChartError'
import { customTheme } from '../../../theme'
import { ThemeProvider } from 'emotion-theming'

describe('ChartError', () => {
  it('matches snapshot', () => {
    const { container } = render(
      <ThemeProvider theme={customTheme}>
        <ChartError errorMessage="Sorry, chart could not be loaded." />
      </ThemeProvider>
    )
    expect(container).toMatchSnapshot()
  })
})