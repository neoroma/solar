import React from 'react'
import { render, cleanup } from '@testing-library/react'
import { SolarPanelListSummary } from './SolarPanelListSummary'

describe('SolarPanelListSummary', () => {
  afterEach(cleanup);
  
  it('matches snapshot', () => {
    const { container } = setup()
    expect(container).toMatchSnapshot()
  })

  it('has correct text', () => {
    const { getByTestId } = setup()
    expect(getByTestId('total-output-text').textContent).toBe('Total output (kW)')
  })

  it('has correct total output', () => {
    const { getByTestId } = setup()
    expect(getByTestId('total-output-data').textContent).toBe('3.44')
  })
})

function setup(props) {
  const defaultProps = {
    totalOutput: 3.44
  }

  const updatedProps = { ...defaultProps, ...props }
  const utils = render(<SolarPanelListSummary {...updatedProps} />)

  return {
    ...utils
  }
}