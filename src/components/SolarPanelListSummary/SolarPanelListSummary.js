import React from 'react'
import PropTypes from 'prop-types'
import { Box } from '@chakra-ui/core'

export function SolarPanelListSummary({ totalOutput }) {
  return (
    <Box
      textAlign="center"
      p={2}>
      <Box data-testid="total-output-text">
        Total output (kW)
      </Box>
      <Box data-testid="total-output-data">
        {totalOutput}
      </Box>
    </Box>
  )
}

SolarPanelListSummary.propTypes = {
  totalOutput: PropTypes.number.isRequired
}