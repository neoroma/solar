import React from 'react'
import { SolarPanel } from '../SolarPanel/SolarPanel'
import PropTypes from 'prop-types'
import { Flex } from "@chakra-ui/core";

export function SolarPanelList({ solarPanels }) {
  return (
    <Flex
      alignItems="center"
      justifyContent="center"
      p={1}
      flexDirection={['column', 'row']}
      flexWrap={['nowrap', 'wrap']}
      data-testid="solar-panel-list">
      {solarPanels.map((solarPanel) => (
        <SolarPanel 
          key={solarPanel.id} 
          {...solarPanel} />
      ))}
    </Flex>
  )
}

SolarPanelList.propTypes = {
  solarPanels: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      voltage: PropTypes.number.isRequired,
      wattage:  PropTypes.number.isRequired
    })
  )
}

