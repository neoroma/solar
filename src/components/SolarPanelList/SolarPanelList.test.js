import React from 'react'
import { render, cleanup } from '@testing-library/react'
import { SolarPanelList } from './SolarPanelList'

describe('SolarPanelList', () => {
  afterEach(cleanup);

  it('matches snapshot', () => {
    const { container } = setup()
    expect(container).toMatchSnapshot()
  })

  it('has correct length', () => {
    const { queryByTestId } = setup()
    const list = queryByTestId('solar-panel-list')
    expect(list.children.length).toBe(2)
  })
})

function setup(props) {
  const defaultProps = {
    solarPanels: [
      {
        id: 'sp-1',
        voltage: 244,
        wattage: 73
      },
      {
        id: 'sp-2',
        voltage: 214,
        wattage: 30
      }
    ]
  }

  const updatedProps = { ...defaultProps, ...props }
  const utils = render(<SolarPanelList {...updatedProps} />)

  return {
    ...utils
  }
}