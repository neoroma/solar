import React from 'react'
import PropTypes from 'prop-types'
import {
  ResponsiveContainer, 
  LineChart, 
  Line, 
  XAxis, 
  YAxis
} from 'recharts';
import { 
  getCloudCoverage, 
  getXAxisDomain, 
  getXAxisTicks,
  tickFormatter,
} from '../../utils/common'
import { planetOsApi, cloudCoverageParams } from '../../config'
import { useRequest } from '../../hooks/useRequest';
import { ChartError } from '../common/ChartError/ChartError';

export function CloudCoverageChart({ height, tickInterval }) {
  const { url, query, pollInterval } = planetOsApi
  const requestUrl = [url, query].join('')
  
  const { isFetching, error, data } = useRequest(
    requestUrl,
    cloudCoverageParams,
    pollInterval
  )

  if (isFetching) {
    return <div>Fetching</div>
  }

  if (error) {
    return <ChartError errorMessage="Sorry, chart could not be loaded."/>
  }

  const chartData = getCloudCoverage(data.entries)
  const ticks = getXAxisTicks(25)
  const { domainStart, domainEnd } = getXAxisDomain(chartData)
  
  return (
    <div style={{ height }}>
      <ResponsiveContainer 
        height={height} 
        width="100%">
        <LineChart
          height={height}
          data={chartData}
          margin={{ top: 20, right: 30, bottom: 20, left: 0 }}
        >
          <XAxis 
            dataKey="time"
            type="number"
            interval={tickInterval}
            domain={[domainStart, domainEnd]}
            tickFormatter={tickFormatter}
            ticks={ticks}/>
          <YAxis dataKey="percentage"/>
          <Line 
            isAnimationActive={false} 
            type="monotone" 
            dataKey="percentage" 
            stroke="#8884d8" 
            strokeDasharray="5 5" />
        </LineChart>
      </ResponsiveContainer>
    </div>
  )
}

CloudCoverageChart.defaultProps = {
  height: 250
}

CloudCoverageChart.propTypes = {
  height: PropTypes.number.isRequired,
  tickInterval: PropTypes.number.isRequired,
}