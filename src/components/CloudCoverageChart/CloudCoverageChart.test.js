import React from "react";
import { render, act } from "@testing-library/react";
import { CloudCoverageChart } from './CloudCoverageChart'
import axios from "axios";
import { cloudCoverageMock } from '../../__mocks__'

describe('CloudCoverageChart', () => {
  it("calls axios on render", async () => {

    axios.get.mockResolvedValueOnce({
      data: {
        entries: cloudCoverageMock.entries
      }
    });
  
    await act(async () => {
      render(<CloudCoverageChart tickInterval={2} height={250}/>);
    })
  
    expect(axios.get).toHaveBeenCalledTimes(1);
  });  
}) 
