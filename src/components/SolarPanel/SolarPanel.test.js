import React from 'react'
import { render, cleanup } from '@testing-library/react'
import { SolarPanel } from './SolarPanel'

describe('SolarPanel', () => {
  afterEach(cleanup);

  it('matches snapshot', () => {
    const { container } = setup()
    expect(container).toMatchSnapshot()
  })

  it('has correct text for voltage', () => {
    const { getByTestId } = setup()
    expect(getByTestId('solar-panel-voltage').textContent).toBe('244 v')
  })

  it('has correct text for wattage', () => {
    const { getByTestId } = setup()
    expect(getByTestId('solar-panel-wattage').textContent).toBe('73 w')
  })
})

function setup(props) {
  const defaultProps = {
    id: 'sp-1',
    voltage: 244,
    wattage: 73
  }
  const updatedProps = { ...defaultProps, ...props }
  const utils = render(<SolarPanel {...updatedProps} />)

  return {
    ...utils
  }
}