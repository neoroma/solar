import React from 'react'
import PropTypes from 'prop-types'
import { Flex } from "@chakra-ui/core";

export function SolarPanel({ 
  id,
  voltage, 
  wattage, 
}) {
  return (
    <Flex 
      data-testid="solar-panel"
      justifyContent="space-between"
      alignItems="center"
      backgroundColor="gray.100"
      borderRadius={2}
      p={2}
      m={1}
      flex="1"
      width="100%"
      minWidth="12rem">
      <Flex flex="1">{id}</Flex>
      <Flex flex="1" data-testid="solar-panel-voltage">
        {voltage} v
      </Flex>
      <Flex flex="1" data-testid="solar-panel-wattage">
        {wattage} w
      </Flex>
    </Flex>
  )
}

SolarPanel.propTypes = {
  id: PropTypes.string.isRequired,
  voltage: PropTypes.number.isRequired,
  wattage: PropTypes.number.isRequired,
}