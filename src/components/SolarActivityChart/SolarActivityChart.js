import React from 'react'
import PropTypes from 'prop-types'
import {
  ResponsiveContainer, 
  LineChart, 
  Line, 
  YAxis, 
  XAxis
} from 'recharts';
import { 
  getSolarActivity, 
  getXAxisDomain, 
  getXAxisTicks, 
  tickFormatter 
} from '../../utils/common'
import { useRequest } from '../../hooks/useRequest'
import { planetOsApi, solarActivityParams } from '../../config'
import { ChartError } from '../common/ChartError/ChartError';

export function SolarActivityChart({ height, tickInterval }) {
  const { url, query, pollInterval } = planetOsApi
  const requestUrl = [url, query].join('')
  
  const { isFetching, error, data } = useRequest(
    requestUrl,
    solarActivityParams,
    pollInterval
  )
  
  if (isFetching) {
    return <div>Fetching</div>
  }

  if (error) {
    return <ChartError errorMessage="Sorry, chart could not be loaded."/>
  }

  const chartData = getSolarActivity(data.entries)
  const ticks = getXAxisTicks(25)
  const { domainStart, domainEnd } = getXAxisDomain(chartData)

  return (
    <div style={{ height }}>
      <ResponsiveContainer 
        height={height} 
        width="100%">
        <LineChart 
          height={height} 
          data={chartData} 
          margin={{ top: 20, right: 30, bottom: 20, left: 0 }}>
        <YAxis dataKey="watts"/>
        <XAxis
          dataKey="time"
          type="number"
          interval={tickInterval}
          domain={[domainStart, domainEnd]}
          tickFormatter={tickFormatter}
          ticks={ticks}/>
        <Line
          isAnimationActive={false}
          type="monotone"
          dataKey="watts"
          stroke="#8884d8"
          strokeDasharray="5 5"/>
        </LineChart>
      </ResponsiveContainer>
    </div>
  )
}

SolarActivityChart.defaultProps = {
  height: 250
}

SolarActivityChart.propTypes = {
  height: PropTypes.number.isRequired,
  tickInterval: PropTypes.number.isRequired,
}