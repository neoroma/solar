import React from "react";
import { render, act } from "@testing-library/react";
import { SolarActivityChart } from './SolarActivityChart'
import axios from "axios";
import { solarActivityMock } from '../../__mocks__'

describe('CloudCoverageChart', () => {
  it("calls axios on render", async () => {

    axios.get.mockResolvedValueOnce({
      data: {
        entries: solarActivityMock.entries
      }
    });
  
    await act(async () => {
      render(<SolarActivityChart tickInterval={2} height={250}/>);
    })
  
    expect(axios.get).toHaveBeenCalledTimes(1);
  });  
}) 
